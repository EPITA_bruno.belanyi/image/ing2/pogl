#version 400

layout(location = 1) in vec3 vPos;

out vec2 texCoords;

uniform vec3 particlePos;

void main() {
    vec3 squarePos = vec3(vPos.x * (9./16.), vPos.yz);
    gl_Position = vec4(squarePos * 0.03 + particlePos, 1.0);
    texCoords = vPos.xy;
}
