use std::ffi::{c_void, CString};
use std::thread;
use std::time::{Duration, Instant};
use std::{mem, ptr, str};

use gl::types::*;

use glutin::dpi::LogicalSize;
use glutin::event::{Event, WindowEvent};
use glutin::event_loop::{ControlFlow, EventLoop};
use glutin::window::WindowBuilder;
use glutin::ContextBuilder;

use image::{DynamicImage, ImageFormat};

mod particles;
use particles::{spawn_particles, Particle};

const WIDTH: usize = 1280;
const HEIGHT: usize = 720;

static VS_SRC: &'static str = include_str!("vertex.glsl");

static FS_SRC: &'static str = include_str!("frag.glsl");

fn check_error() {
    let err = unsafe { gl::GetError() };
    match err {
        gl::NO_ERROR => (),
        gl::INVALID_ENUM => println!("INVALID_ENUM"),
        gl::INVALID_VALUE => println!("INVALID_VALUE"),
        gl::INVALID_OPERATION => println!("INVALID_OPERATION"),
        gl::INVALID_FRAMEBUFFER_OPERATION => println!("INVALID_FRAMEBUFFER_OPERATION"),
        gl::OUT_OF_MEMORY => println!("INVALID_OUT_OF_MEMORY"),
        gl::STACK_UNDERFLOW => println!("INVALID_STACK_UNDERFLOW"),
        gl::STACK_OVERFLOW => println!("INVALID_STACK_OVERFLOW"),
        _ => println!("unknown error"),
    }
}

fn compile_shader(src: &str, ty: GLenum) -> GLuint {
    let shader;
    unsafe {
        shader = gl::CreateShader(ty);
        let c_str = CString::new(src.as_bytes()).unwrap();
        gl::ShaderSource(shader, 1, &c_str.as_ptr(), ptr::null());
        gl::CompileShader(shader);

        // error checking
        let mut status = gl::FALSE as GLint;
        gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut status);
        if status != (gl::TRUE as GLint) {
            let mut len = 0;
            gl::GetShaderiv(shader, gl::INFO_LOG_LENGTH, &mut len);
            let mut buf = Vec::with_capacity(len as usize);
            // ignore the trailing null byte in buffer
            buf.set_len((len as usize) - 1);
            gl::GetShaderInfoLog(
                shader,
                len,
                ptr::null_mut(),
                buf.as_mut_ptr() as *mut GLchar,
            );
            panic!(
                "{}",
                str::from_utf8(&buf)
                    .ok()
                    .expect("ShaderInfoLog not valid utf8")
            );
        }
    }
    shader
}

fn link_program(vs: GLuint, fs: GLuint) -> GLuint {
    let program;
    unsafe {
        program = gl::CreateProgram();
        gl::AttachShader(program, vs);
        gl::AttachShader(program, fs);
        gl::LinkProgram(program);

        // error checking
        let mut status = gl::FALSE as GLint;
        gl::GetProgramiv(program, gl::LINK_STATUS, &mut status);
        if status != (gl::TRUE as GLint) {
            let mut len: GLint = 0;
            gl::GetProgramiv(program, gl::INFO_LOG_LENGTH, &mut len);
            let mut buf = Vec::with_capacity(len as usize);
            // ignore the trailing null byte in buffer
            buf.set_len((len as usize) - 1);
            gl::GetProgramInfoLog(
                program,
                len,
                ptr::null_mut(),
                buf.as_mut_ptr() as *mut GLchar,
            );
            panic!(
                "{}",
                str::from_utf8(&buf)
                    .ok()
                    .expect("ProgramInfoLog not valid utf8")
            );
        }
    }
    program
}

fn init_data() -> GLuint {
    let mut vao = 0;
    let mut triangle_vertex_vbo = 0;

    let triangle_vertex_list: [GLfloat; 12] = [0., 0., 0., 1., 0., 0., 0., 1., 0., 1., 1., 0.];

    unsafe {
        gl::GenVertexArrays(1, &mut vao);
        gl::BindVertexArray(vao);

        gl::GenBuffers(1, &mut triangle_vertex_vbo);
        gl::BindBuffer(gl::ARRAY_BUFFER, triangle_vertex_vbo);
        gl::BufferData(
            gl::ARRAY_BUFFER,
            (3 * 4 * mem::size_of::<GLfloat>()) as GLsizeiptr,
            mem::transmute(&triangle_vertex_list[0]),
            gl::STATIC_DRAW,
        );

        gl::EnableVertexAttribArray(1);
        gl::VertexAttribPointer(1, 3, gl::FLOAT, gl::FALSE, 0, ptr::null());

        gl::BindVertexArray(0);
    }

    vao
}

fn load_texture() -> GLuint {
    let img_bytes = include_bytes!("../particle.png");
    let img = image::load_from_memory_with_format(img_bytes, ImageFormat::Png)
        .expect("couldn't load image");
    let img = match img {
        DynamicImage::ImageRgba8(img) => img,
        img => img.to_rgba(),
    };

    let (width, height) = (img.width() as i32, img.height() as i32);
    let data = img.into_raw();

    let mut tid: GLuint = 0;
    unsafe {
        gl::GenTextures(1, &mut tid);
        gl::BindTexture(gl::TEXTURE_2D, tid);
        gl::TexImage2D(
            gl::TEXTURE_2D,
            0,
            gl::RGBA as i32,
            width,
            height,
            0,
            gl::RGBA,
            gl::UNSIGNED_BYTE,
            data.as_ptr() as *const c_void,
        );
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as i32);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as i32);
    }

    check_error();

    tid
}

fn init_particles() -> Vec<Particle> {
    let mut particles = spawn_particles(1, [-0.25, -0.5, 0.0], [0.25, -0.5, 0.0]);

    for _ in 0..40 {
        for p in particles.iter_mut() {
            p.animate();
        }
        particles.append(&mut spawn_particles(
            100,
            [-0.25, -0.5, 0.0],
            [0.25, -0.5, 0.0],
        ));
    }

    particles
}

fn main() {
    let el = EventLoop::new();
    let wb = WindowBuilder::new()
        .with_title("glow")
        .with_inner_size(LogicalSize::new(WIDTH as f32, HEIGHT as f32));
    let windowed_context = ContextBuilder::new()
        .with_gl(glutin::GlRequest::Specific(glutin::Api::OpenGl, (4, 0)))
        .build_windowed(wb, &el)
        .unwrap();

    let gl_window = unsafe { windowed_context.make_current().unwrap() };

    gl::load_with(|symbol| gl_window.get_proc_address(symbol));

    unsafe {
        gl::Disable(gl::DEPTH_TEST);
        gl::Disable(gl::CULL_FACE);
        gl::Enable(gl::BLEND);
        gl::BlendFunc(gl::SRC_ALPHA, gl::ONE);
        gl::Viewport(0, 0, WIDTH as i32, HEIGHT as i32);
        gl::ClearColor(0.0, 0.0, 0.0, 1.0);
    }

    let _ = load_texture();
    let vs = compile_shader(VS_SRC, gl::VERTEX_SHADER);
    let fs = compile_shader(FS_SRC, gl::FRAGMENT_SHADER);
    let program = link_program(vs, fs);

    let vao_id = init_data();

    let temperature_location =
        unsafe { gl::GetUniformLocation(program, CString::new("particleTemp").unwrap().as_ptr()) };
    let position_location =
        unsafe { gl::GetUniformLocation(program, CString::new("particlePos").unwrap().as_ptr()) };

    let mut particles = init_particles();

    let mut last_draw = Instant::now();

    el.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;
        match event {
            Event::LoopDestroyed => return,
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::Resized(physical_size) => gl_window.resize(physical_size),
                WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                _ => (),
            },
            Event::MainEventsCleared => {
                let elapsed = last_draw.elapsed().as_millis();

                const INTERVAL: u128 = 16;
                if elapsed >= INTERVAL {
                    last_draw = Instant::now();
                    unsafe {
                        gl::UseProgram(program);
                        gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
                        gl::BindVertexArray(vao_id);

                        for particle in particles.iter_mut() {
                            let coords = particle.coordinates;
                            let temp = particle.temperature;
                            gl::Uniform3f(position_location, coords[0], coords[1], coords[2]);
                            gl::Uniform1ui(temperature_location, temp);
                            gl::DrawArrays(gl::TRIANGLE_STRIP, 0, 4);
                            particle.animate();
                        }

                        gl::BindVertexArray(0);
                        gl::Finish();
                        check_error();
                    }
                    gl_window.swap_buffers().unwrap();
                } else {
                    thread::sleep(Duration::from_millis((INTERVAL - elapsed) as u64));
                }
            }
            _ => (),
        }
    });
}
