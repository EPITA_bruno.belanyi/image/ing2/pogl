use gl::types::*;
use rand::Rng;

pub struct Particle {
    /// Spawning point of the particle.
    origin: [GLfloat; 3],
    /// Where the particle should be headed.
    endpoint: [GLfloat; 3],
    /// Initial velocity.
    init_v: [GLfloat; 3],
    /// 3D coordinates of the particle.
    pub coordinates: [GLfloat; 3],
    /// Temperature correlates with color.
    pub temperature: GLuint,
}

/// Initial temperature of a particle when spawned,
const INITIAL_TEMPERATURE: GLuint = 38;

/// How much the maximum step of an animation step can move us by.
const DELTA_STEP: GLfloat = 0.05; // FIXME: correctness?

/// When should we respawn the particle.
const HEIGHT_CUTOFF: GLfloat = 0.75; // FIXME: correctness?

// FIXME: correctness?
/// Which index corresponds to which axis.
const HEIGHT_INDEX: usize = 1;
const WIDTH_INDEX: usize = 0;
const DEPTH_INDEX: usize = 2;

/// How many steps will the particle move laterally.
const VELOCITY_STEPS: GLuint = 5;

impl Particle {
    pub fn new(coordinates: [f32; 3], init_v: [f32; 3], endpoint: [GLfloat; 3]) -> Self {
        Particle {
            origin: coordinates,
            endpoint,
            init_v,
            coordinates,
            temperature: INITIAL_TEMPERATURE,
        }
    }

    /// Animate the particle through one step of the simulation
    pub fn animate(&mut self) {
        if self.coordinates[HEIGHT_INDEX] >= HEIGHT_CUTOFF || self.temperature == 0 {
            self.coordinates = self.origin;
            self.temperature = INITIAL_TEMPERATURE;
            return;
        }

        let mut rng = rand::thread_rng();

        self.coordinates[HEIGHT_INDEX] += rng.gen_range(0.0, DELTA_STEP) * 0.5;
        self.temperature -= 1;

        let vel_multiplier =
            VELOCITY_STEPS.saturating_sub(INITIAL_TEMPERATURE - self.temperature) as GLfloat;
        if vel_multiplier > 0. {
            self.coordinates[WIDTH_INDEX] += self.init_v[WIDTH_INDEX] * DELTA_STEP * vel_multiplier;
            self.coordinates[DEPTH_INDEX] += self.init_v[DEPTH_INDEX] * DELTA_STEP * vel_multiplier;
        } else {
            self.coordinates[WIDTH_INDEX] +=
                (self.endpoint[WIDTH_INDEX] - self.coordinates[WIDTH_INDEX]) * DELTA_STEP;
            self.coordinates[DEPTH_INDEX] +=
                (self.endpoint[DEPTH_INDEX] - self.coordinates[DEPTH_INDEX]) * DELTA_STEP;
        }
    }
}

pub fn spawn_particles(n: usize, begin: [GLfloat; 3], end: [GLfloat; 3]) -> Vec<Particle> {
    let step = [
        (end[0] - begin[0]) / (n as GLfloat),
        (end[1] - begin[1]) / (n as GLfloat),
        (end[2] - begin[2]) / (n as GLfloat),
    ];

    let endpoint = [
        begin[0] + (end[0] - begin[0]) / (2 as GLfloat),
        10.,
        begin[2] + (end[2] - begin[2]) / (2 as GLfloat),
    ];

    let mut res = Vec::with_capacity(n);
    let mut rng = rand::thread_rng();

    for i in 0..n {
        let p = Particle::new(
            [
                begin[0] + (i as GLfloat) * step[0],
                begin[1] + (i as GLfloat) * step[1],
                begin[2] + (i as GLfloat) * step[2],
            ],
            [
                rng.gen_range(-DELTA_STEP, DELTA_STEP),
                0.,
                rng.gen_range(-DELTA_STEP, DELTA_STEP),
            ],
            endpoint,
        );
        res.push(p);
    }

    res
}
